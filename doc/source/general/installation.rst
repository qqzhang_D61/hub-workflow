============
Installation
============

Install the latest release with pip::

    python -m pip install https://bitbucket.org/hu-geomatics/hub-workflow/get/master.tar.gz

Or manually `download a release <https://bitbucket.org/hu-geomatics/hub-workflow/downloads/?tab=tags>`_
from Bitbucket.
